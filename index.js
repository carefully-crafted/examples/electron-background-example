const { app, BrowserWindow, Tray, Menu, nativeImage } = require("electron");
const path = require("node:path");

let win;
let tray = null;

function createTray() {
  const icon = path.join(__dirname, "/cc.png"); // required.
  const trayicon = nativeImage.createFromPath(icon);
  tray = new Tray(trayicon.resize({ width: 16 }));
  const contextMenu = Menu.buildFromTemplate([
    {
      label: "Show App",
      click: () => {
        createWindow();
      },
    },
    {
      label: "Quit",
      click: () => {
        app.quit();
      },
    },
  ]);

  tray.setContextMenu(contextMenu);
}

const createWindow = () => {
  if (!tray) {
    createTray();
  }

  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
    },
  });

  win.loadFile("index.html");

  win.on("closed", function() {
    win = null;
  });
};

app.whenReady().then(() => {
  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on("window-all-closed", () => {
  // for macOS
  app.dock.hide();
  // TODO: handle windows et al.
});

const startCounter = async () => {
  for (let i = 0; i < 10000; i++) {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    if (win) {
      console.log("sending counter", i);
      win.webContents.postMessage("counter", i);
    }
    if (!win) {
      console.log("no window");
    }
  }
};

async function main() {
  await startCounter();
}

main();
